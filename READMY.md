# Read me

### Acces H2 console
Une foi l'application lancé cliquez sur l'url:

* [http://localhost:8080/h2-console/](http://localhost:8080/h2-console/)

le champs JDBC URL doit etre : jdbc:h2:mem:person_db

### HTTP request 
voici un ensemble de tests a executer

http://localhost:8080/persons

recherche toutes les personnes par un findAll() sans filtre.

http://localhost:8080/persons?search=nom:o

recherche toutes les personnes dont le nom contient un o.

http://localhost:8080/persons?search=nom:l;prenom=i

recherche toutes les personnes dont le nom contient un l et le prenom un i.

http://localhost:8080/persons?age<50

recherche toutes les personnes dont l'age est inférieur et égale à 50 ans.

http://http://localhost:8080/persons?search=age><32,56

recherche toutes les personnes entre 30 et 49 ans compris

http://localhost:8080/persons?search=dateCreation<2017-04-14T11:03:43.841

recherche toutes les personnes insérées avant la date 2017-04-14T11:03:43.841 comprise

