package com.ginus.criteriaFilter.Dao;

import com.ginus.criteriaFilter.persistence.EntitySearchQueryCriteriaConsumer;
import com.ginus.criteriaFilter.persistence.util.SearchCriteria;
import com.ginus.criteriaFilter.models.Person;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class PersonDaoImpl implements PersonDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Person> searchPerson(final List<SearchCriteria> params) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Person> query = builder.createQuery(Person.class);
        final Root r = query.from(Person.class);

        Predicate predicate = builder.conjunction();
        EntitySearchQueryCriteriaConsumer searchConsumer = new EntitySearchQueryCriteriaConsumer(predicate, builder, r);
        params.stream().forEach(searchConsumer);
        predicate = searchConsumer.getPredicate();
        query.where(predicate);

        return entityManager.createQuery(query).getResultList();
    }

}
