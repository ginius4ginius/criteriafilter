package com.ginus.criteriaFilter.Dao;

import com.ginus.criteriaFilter.persistence.util.SearchCriteria;
import com.ginus.criteriaFilter.models.Person;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface PersonDao {

    public List<Person> searchPerson(List<SearchCriteria> params);

}
