package com.ginus.criteriaFilter.controller;

import com.ginus.criteriaFilter.persistence.util.SearchCriteria;
import com.ginus.criteriaFilter.models.Person;
import com.ginus.criteriaFilter.Dao.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ginus.criteriaFilter.persistence.util.DateUtils.TIMESTAMP_REGMATCHER;

@RestController
@CrossOrigin("*")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    // API - SEARCH
    @RequestMapping(method = RequestMethod.GET, value = "/persons")
    @ResponseBody
    public List<Person> search(@RequestParam(value = "search", required = false) String search) {
        List<SearchCriteria> params = new ArrayList<>();
        if (search != null) {
            Pattern pattern = Pattern.compile("(\\w+?)(:|<|>|><)(\\w+?|(\\w+?),\\w+?|("+TIMESTAMP_REGMATCHER+")" +
                    "|("+TIMESTAMP_REGMATCHER+"),("+TIMESTAMP_REGMATCHER+"));");
            Matcher matcher = pattern.matcher(search + ";");
            while (matcher.find()) {
                params.add(new SearchCriteria(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
        }
        return personRepository.searchPerson(params);
    }

    // API - READ
    @GetMapping(value = "/person/{id}")
    public Person getPerson(@PathVariable("id") Long id) {
        Optional<Person> p = personRepository.findById(id);

        if (!p.isPresent()) {
            throw new RuntimeException("Aucun role trouvé");
        }

        return p.get();

    }

    // API - WRITE
    @PostMapping(value = "/person")
    public Person create(@RequestBody Person p) {
        return personRepository.save(p);
    }



}
