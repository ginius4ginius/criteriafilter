package com.ginus.criteriaFilter;

import com.ginus.criteriaFilter.models.Person;
import com.ginus.criteriaFilter.Dao.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;

@SpringBootApplication
public class CriteriaFilterApplication  implements CommandLineRunner {

	@Autowired
	PersonRepository personRepository;

	public static void main(String[] args) {
		SpringApplication.run(CriteriaFilterApplication.class, args);
	}

	@Override
	public void run(String... args){

		LocalDateTime date = LocalDateTime.now(); //2020
		LocalDateTime date2 =  LocalDateTime.now().minusYears(9);//2011
		LocalDateTime date3 =  LocalDateTime.now().minusYears(5);//2015
		LocalDateTime date4 =  LocalDateTime.now().minusYears(3);//2017

		Person kim = new Person("kardashian", "kim", date,32);
		Person luism = new Person("mariano", "luis", date2,64);
		Person luisr = new Person("ronaldo", "luis", date3,56);
		Person cristiano = new Person("ronaldo", "cristiano", date4,48);

		personRepository.save(kim);
		personRepository.save(luism);
		personRepository.save(luisr);
		personRepository.save(cristiano);

	}
}
