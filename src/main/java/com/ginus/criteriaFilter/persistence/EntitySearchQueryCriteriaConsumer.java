package com.ginus.criteriaFilter.persistence;

import com.ginus.criteriaFilter.persistence.util.DateUtils;
import com.ginus.criteriaFilter.persistence.util.SearchCriteria;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.function.Consumer;

public class EntitySearchQueryCriteriaConsumer implements Consumer<SearchCriteria> {

    private Predicate predicate;
    private CriteriaBuilder builder;
    private Root r;

    public EntitySearchQueryCriteriaConsumer(Predicate predicate, CriteriaBuilder builder, Root r) {
        super();
        this.predicate = predicate;
        this.builder = builder;
        this.r = r;
    }

    @Override
    public void accept(SearchCriteria param) {

        final Path path = r.get(param.getKey());

        if (param.getOperation().equalsIgnoreCase(">")) {// traitement opérateur SUPERIEUR ou EGALE
            if (DateUtils.isTimeStamp(param.getValue().toString())) {
                LocalDateTime date = DateUtils.transformStringToLocalDateTime(param.getValue().toString());
                predicate = builder.and(predicate, builder.greaterThanOrEqualTo(path,
                        date));
            } else {
                predicate = builder.and(predicate, builder.greaterThanOrEqualTo(path, param.getValue().toString()));
            }

        } else if (param.getOperation().equalsIgnoreCase("<")) {// traitement opérateur INFERIEUR ou EGALE
            if (DateUtils.isTimeStamp(param.getValue().toString())) {
                LocalDateTime date = DateUtils.transformStringToLocalDateTime(param.getValue().toString());
                predicate = builder.and(predicate, builder.lessThanOrEqualTo(path,
                        date));
            } else {
                predicate = builder.and(predicate, builder.lessThanOrEqualTo(path, param.getValue().toString()));
            }

        } else if (param.getOperation().equalsIgnoreCase("><")) {// traitement opérateur ENTRE
            String[] arrOfStr = param.getValue().toString().split(",");
            if (DateUtils.isTimeStamp(arrOfStr[0])) {
                LocalDateTime date1 = DateUtils.transformStringToLocalDateTime(arrOfStr[0]);
                LocalDateTime date2 = DateUtils.transformStringToLocalDateTime(arrOfStr[1]);
                predicate = builder.and(predicate, builder.between(path, date1,
                        date2));
            } else {
                predicate = builder.and(predicate, builder.between(path, Double.parseDouble(arrOfStr[0]),
                        Double.parseDouble(arrOfStr[1])));
            }

        } else if (param.getOperation().equalsIgnoreCase(":")) {// traitement opérateur LIKE
            if (DateUtils.isTimeStamp(param.getValue().toString())) {
                LocalDateTime date = DateUtils.transformStringToLocalDateTime(param.getValue().toString());
                predicate = builder.and(predicate, builder.equal(path,
                        date));
            } else if (r.get(param.getKey()).getJavaType() == String.class) {
                predicate = builder.and(predicate, builder.like(path, "%" + param.getValue() + "%"));
            } else {
                predicate = builder.and(predicate, builder.equal(path, param.getValue()));
            }
        }
    }

    public Predicate getPredicate() {
        return predicate;
    }

}
