package com.ginus.criteriaFilter.persistence.util;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtils {

    public static final String TIMESTAMP_REGMATCHER = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}";

    /**
     * Méthode permettant de retourner si le paramètre de type String est de format de type Timestamp.
     * @param param de type String
     * @return boolean true si match
     */
    public static boolean isTimeStamp(String param) {

        Pattern pattern = Pattern.compile(TIMESTAMP_REGMATCHER);
        Matcher matcher = pattern.matcher(param);

        return matcher.matches();
    }

    /**
     * Méthode transformant un string en format localDateTime.
     * Methode qui fonctionne à la condition que la chaine de caractères est bien dans un format d'un localDateTime
     * @param param de type String
     * @return un objet de type localDateTime.
     */
    public static LocalDateTime transformStringToLocalDateTime(String param) {
        return LocalDateTime.parse(param);
    }

}
